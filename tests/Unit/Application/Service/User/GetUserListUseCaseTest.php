<?php

namespace Tests\Unit\Application\Service\User;

use Application\Service\User\GetUserListUseCase;
use Domain\Entity\User\UserCollection;
use Domain\Repositories\JsonUserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetUserListUseCaseTest extends TestCase
{
    /** @var JsonUserRepository|MockObject */
    private $jsonUserRepository;

    /** @var GetUserListUseCase */
    private GetUserListUseCase $sut;

    protected function setUp(): void
    {
        parent::setUp();
        $this->jsonUserRepository = $this->createMock(JsonUserRepository::class);

        $this->sut = new GetUserListUseCase($this->jsonUserRepository);
    }

    public function testWhenUserListMustBeReturned(): void
    {
        $limit = 20;
        $userCollection = new UserCollection();
        $this->jsonUserRepository
            ->expects(self::once())
            ->method('getUserList')
            ->with($limit)
            ->willReturn($userCollection);

        $actual = $this->sut->execute($limit);
        self::assertEquals($userCollection, $actual);
    }
}

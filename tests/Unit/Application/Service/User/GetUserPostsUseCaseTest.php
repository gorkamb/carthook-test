<?php

namespace Tests\Unit\Application\Service\User;

use Application\Service\User\GetUserPostsUseCase;
use Domain\Entity\Post\PostCollection;
use Domain\Repositories\JsonPostRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetUserPostsUseCaseTest extends TestCase
{
    /** @var JsonPostRepository|MockObject */
    private $jsonPostRepository;

    /** @var GetUserPostsUseCase */
    private GetUserPostsUseCase $sut;

    protected function setUp(): void
    {
        parent::setUp();
        $this->jsonPostRepository = $this->createMock(JsonPostRepository::class);

        $this->sut = new GetUserPostsUseCase($this->jsonPostRepository);
    }

    public function testWhenUserPostMustBeReturned(): void
    {
        $userId = 1;
        $limit = 20;
        $postCollection = new PostCollection();
        $this->jsonPostRepository
            ->expects(self::once())
            ->method('getPostsByUserId')
            ->with($userId, $limit)
            ->willReturn($postCollection);

        $actual = $this->sut->execute($userId, $limit);
        self::assertEquals($postCollection, $actual);
    }
}
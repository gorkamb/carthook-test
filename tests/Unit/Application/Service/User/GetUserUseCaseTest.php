<?php


namespace Tests\Unit\Application\Service\User;


use Application\Service\User\GetUserUseCase;
use Domain\Entity\User\User;
use Domain\Repositories\JsonUserRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetUserUseCaseTest extends TestCase
{

    /** @var JsonUserRepository|MockObject */
    private $jsonUserRepository;

    /** @var GetUserUseCase */
    private GetUserUseCase $sut;

    protected function setUp(): void
    {
        parent::setUp();
        $this->jsonUserRepository = $this->createMock(JsonUserRepository::class);

        $this->sut = new GetUserUseCase($this->jsonUserRepository);
    }

    public function testUserMustBeReturned(): void
    {
        $userId = 1;
        $user = new User();

        $this->jsonUserRepository
            ->expects(self::once())
            ->method('getUserById')
            ->with($userId)
            ->willReturn($user);

        $actual = $this->sut->execute($userId);
        self::assertEquals($user, $actual);
    }


}
<?php

namespace Tests\Unit\Application\Service\Post;

use Application\Service\Post\GetPostCommentsUseCase;
use Domain\Entity\Comment\CommentCollection;
use Domain\Repositories\JsonCommentRepository;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

class GetPostCommentsUseCaseTest extends TestCase
{

    /** @var JsonCommentRepository|MockObject */
    private $jsonCommentRepository;

    /** @var GetPostCommentsUseCase */
    private GetPostCommentsUseCase $sut;

    protected function setUp(): void
    {
        parent::setUp();

        $this->jsonCommentRepository = $this->createMock(JsonCommentRepository::class);
        $this->sut = new GetPostCommentsUseCase($this->jsonCommentRepository);
    }

    public function testWhenCommentsByPostMustBeReturn(): void
    {
        $postId = 1;
        $limit = 20;

        $commentCollection = new CommentCollection();
        $this->jsonCommentRepository
            ->expects(self::once())
            ->method('getCommentsByPostId')
            ->with($postId, $limit)
            ->willReturn($commentCollection);

        $actual = $this->sut->execute($postId, $limit);
        self::assertEquals($commentCollection, $actual);
    }
}

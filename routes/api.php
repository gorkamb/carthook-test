<?php

use App\Http\Controllers\Post\GetPostCommentsController;
use App\Http\Controllers\User\GetUserController;
use App\Http\Controllers\User\GetUserListController;
use App\Http\Controllers\User\GetUserPostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('api')->group(static function () {
    Route::group(['prefix' => '/users'], static function () {
        Route::get('', GetUserListController::class)->name('users.list');
        Route::get('/{id}', GetUserController::class)->name('users.show');
        Route::get('/{id}/posts', GetUserPostController::class)->name('users.posts.list');
    });

    Route::group(['prefix' => '/posts'], static function () {
        Route::get('/{id}/comments', GetPostCommentsController::class)->name('posts.comments');
    });
});

## CartHook test
- Basic Cs answer are in basic_cs folder (I didn't understand well the goal in the c and d questions. Sorry!) 
- The advanced answers is more detailed 
##### Requirements
 - PHP 7.4
 - ext-json
 
##### Setup
Execute in root folder:
```
composer install
cp .env.example env
```
Adjust database configuration in .env file. and execute:
``` 
php artisan migrate
```
Import JsonPlaceholder data to database:
``` 
php artisan jsonplaceholder:import
``` 

**Note:** I added in "resources" folder the postman collection if you want to test endpoints easily. 

##### Information and Advanced/Practical answer
The main code is in "src" folder as Hexagonal Architecture. The main reason
to use it is because clean code and testing. The code is PSR12 compliant.

The controller has only invoke method to keep tiny because is the entry point to application service.
I use the UseCase suffix application services, that always they have only a one public method for Single Responsibility.
In the application service we can call any other service or repository. I added interfaces for each repository
to keep the possibility to change it easily... The controller use laravel flugger package for response. We
can change it for the "new" laravel resources instead use the transformers. Anyway the data to use is
standard.

For this project Hexagonal Architecture is practical for organization. But I can work as "laravel" or other mode too :). 
I did test for application services.


I created pipeline for bitbucket, so you can see that passes (I use them in all projects):
 - phpcs
 - larastan
 - phpunit
 
**NOTE**: The import command from json placeholder can be optimized using laravel chunk method or more import using raw and bulk
insert.  
 
###### Users table 
- I didn't add address and company info to users table. We can have users_address and users_company tables for that.
- The email field indexed for performance
- The username field limited to 80 chars (255 seems too much...)
- The phone field limited to 30 chars (Maybe 15 digits will be better)

###### Posts table
- The title field indexed for performance
- user_id field as foreign key

###### Comments table
- The email field indexed for performance
- post_id field as foreign key

##### Potential optimizations
- Base on business logic the user extra data info, address and company data can be in separated tables.
- For better search in text fields (email, title) we can use TNT search for that.  https://github.com/teamtnt/laravel-scout-tntsearch-driver
or use elasticsearch
- We can use redis for cache to obtain better performance



 
 






<?php

namespace App\Console\Commands;

use Domain\Service\JSONPlaceholder\ImportInitialDataService;
use Illuminate\Console\Command;
use Throwable;

class ImportFromJsonPlaceholderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jsonplaceholder:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '
    Import data from json placeholder api.
    We assumed that our database is empty
    and we want populate data as is.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ImportInitialDataService $importInitialDataService
     * @return void
     */
    public function handle(ImportInitialDataService $importInitialDataService): void
    {
        $this->info('Starting import');
        try {
            $importInitialDataService->execute();
            $this->info('Imported');
        } catch (Throwable $e) {
            $this->error('Something was wrong...' . $e->getMessage());
        }
    }
}

<?php

namespace App\Http\Transformers\User;

use Domain\Entity\User\User;
use Flugg\Responder\Transformers\Transformer;

class UserTransformer extends Transformer
{
    public function transform(User $user): array
    {
        return [
            'id' => $user->id(),
            'name' => $user->name(),
            'username' => $user->username(),
            'email' => $user->email(),
            'phone' => $user->phone(),
            'website' => $user->website(),
            'address' => [
                'street' => $user->address()->street(),
                'suite' => $user->address()->suite(),
                'city' => $user->address()->city(),
                'zipcode' => $user->address()->zipcode(),
                'geo' => [
                    'lat' => $user->address()->geo()->lat(),
                    'lng' => $user->address()->geo()->lng()
                ]
            ],
            'company' => [
                'name' => $user->company()->name(),
                'catchPhrase' => $user->company()->catchPhrase(),
                'bs' => $user->company()->bs()
            ]
        ];
    }
}

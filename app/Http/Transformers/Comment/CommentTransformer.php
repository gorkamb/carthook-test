<?php

namespace App\Http\Transformers\Comment;

use Domain\Entity\Comment\Comment;
use Flugg\Responder\Transformers\Transformer;

class CommentTransformer extends Transformer
{
    public function transform(Comment $comment): array
    {
        return [
            'id' => $comment->id(),
            'postId' => $comment->postId(),
            'name' => $comment->name(),
            'email' => $comment->email(),
            'body' => $comment->body()
        ];
    }
}

<?php

namespace App\Http\Transformers\Post;

use Domain\Entity\Post\Post;
use Flugg\Responder\Transformers\Transformer;

class PostTransformer extends Transformer
{
    public function transform(Post $post): array
    {
        return [
            'id' => $post->id(),
            'userId' => $post->userId(),
            'title' => $post->title(),
            'body' => $post->body()
        ];
    }
}

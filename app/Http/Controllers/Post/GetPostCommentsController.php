<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Transformers\Comment\CommentTransformer;
use App\Http\Transformers\Post\PostTransformer;
use Application\Service\Post\GetPostCommentsUseCase;
use Flugg\Responder\Contracts\Responder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetPostCommentsController extends Controller
{
    private const DEFAULT_PAGINATION = 10;

    public function __invoke(
        int $postId,
        Request $request,
        Factory $validationFactory,
        Guard $guard,
        Responder $responder,
        GetPostCommentsUseCase $getPostCommentsUseCase
    ): JsonResponse {
        try {
            $limit = (int)$request->input('limit', self::DEFAULT_PAGINATION);
            $comments = $getPostCommentsUseCase->execute($postId, $limit);
            return responder()
                ->success($comments, CommentTransformer::class)
                ->respond(Response::HTTP_OK);
        } catch (Throwable $e) {
            return $responder->error($e->getCode(), $e->getMessage())->respond();
        }
    }
}

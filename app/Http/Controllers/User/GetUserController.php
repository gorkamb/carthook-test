<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Transformers\User\UserTransformer;
use Application\Service\User\GetUserUseCase;
use Flugg\Responder\Contracts\Responder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetUserController extends Controller
{
    public function __invoke(
        int $userId,
        Request $request,
        Factory $validationFactory,
        Guard $guard,
        Responder $responder,
        GetUserUseCase $getUserUseCase
    ): JsonResponse {
        try {
            $user = $getUserUseCase->execute($userId);
            return $responder->success($user, UserTransformer::class)->respond(Response::HTTP_OK);
        } catch (Throwable $e) {
            return $responder->error($e->getCode(), $e->getMessage())->respond();
        }
    }
}

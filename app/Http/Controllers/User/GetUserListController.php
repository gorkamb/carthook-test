<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Transformers\User\UserTransformer;
use Application\Service\User\GetUserListUseCase;
use Flugg\Responder\Contracts\Responder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetUserListController extends Controller
{
    private const DEFAULT_PAGINATION = 10;

    public function __invoke(
        Request $request,
        Factory $validationFactory,
        Guard $guard,
        Responder $responder,
        GetUserListUseCase $getUserListUseCase
    ): JsonResponse {
        try {
            $limit = (int)$request->input('limit', self::DEFAULT_PAGINATION);
            $userList = $getUserListUseCase->execute($limit);
            return responder()
                ->success($userList, UserTransformer::class)
                ->respond(Response::HTTP_OK);
        } catch (Throwable $e) {
            return $responder->error($e->getCode(), $e->getMessage())->respond();
        }
    }
}

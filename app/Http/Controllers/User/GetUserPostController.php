<?php

namespace App\Http\Controllers\User;

use App\Http\Transformers\Post\PostTransformer;
use Application\Service\User\GetUserPostsUseCase;
use Flugg\Responder\Contracts\Responder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GetUserPostController
{
    private const DEFAULT_PAGINATION = 10;

    public function __invoke(
        int $userId,
        Request $request,
        Factory $validationFactory,
        Guard $guard,
        Responder $responder,
        GetUserPostsUseCase $getUserPostsUseCase
    ): JsonResponse {
        try {
            $limit = (int)$request->input('limit', self::DEFAULT_PAGINATION);
            $posts = $getUserPostsUseCase->execute($userId, $limit);
            return responder()
                ->success($posts, PostTransformer::class)
                ->respond(Response::HTTP_OK);
        } catch (Throwable $e) {
            return $responder->error($e->getCode(), $e->getMessage())->respond();
        }
    }
}

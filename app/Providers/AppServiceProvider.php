<?php

namespace App\Providers;

use Domain\Repositories\MysqlCommentRepository;
use Domain\Repositories\MysqlPostRepository;
use Domain\Repositories\MysqlUserRepository;
use Domain\Repositories\JsonCommentRepository;
use Domain\Repositories\JsonPostRepository;
use Domain\Repositories\JsonUserRepository;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Repositories\Eloquent\EloquentCommentRepository;
use Infrastructure\Repositories\Eloquent\EloquentPostRepository;
use Infrastructure\Repositories\Eloquent\EloquentUserRepository;
use Infrastructure\Repositories\JSONPlaceholder\CommentRepository;
use Infrastructure\Repositories\JSONPlaceholder\PostRepository;
use Infrastructure\Repositories\JSONPlaceholder\UserRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(MysqlUserRepository::class, EloquentUserRepository::class);
        $this->app->bind(MysqlPostRepository::class, EloquentPostRepository::class);
        $this->app->bind(MysqlCommentRepository::class, EloquentCommentRepository::class);
        $this->app->bind(JsonUserRepository::class, UserRepository::class);
        $this->app->bind(JsonPostRepository::class, PostRepository::class);
        $this->app->bind(JsonCommentRepository::class, CommentRepository::class);
        $this->app->bind(
            Client::class,
            static function () {
                $defaultOptions = [];
                $defaultOptions['base_uri'] = config('placeholder.url');
                return new Client($defaultOptions);
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}

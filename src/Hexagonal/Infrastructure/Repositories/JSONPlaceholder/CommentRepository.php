<?php

namespace Infrastructure\Repositories\JSONPlaceholder;

use Domain\Entity\Comment\CommentCollection;
use Domain\Repositories\JsonCommentRepository;

class CommentRepository extends BaseRepository implements JsonCommentRepository
{
    final public function getCommentsByPostId(int $postId, int $limit): CommentCollection
    {
        // $limit can be used to limit the result collection in the external api
        // in this case (json_placeholder), we dont have this option...
        $query = [
            'postId' => $postId
        ];
        $response = $this->client->get('comments?' . http_build_query($query));
        $posts = $this->parseResponse($response);
        return CommentCollection::fromPlaceholderData($posts);
    }
}
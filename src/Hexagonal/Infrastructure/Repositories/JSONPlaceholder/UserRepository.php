<?php

namespace Infrastructure\Repositories\JSONPlaceholder;

use Domain\Entity\User\User;
use Domain\Entity\User\UserCollection;
use Domain\Exceptions\DataNotFoundException;
use Domain\Repositories\JsonUserRepository;
use JsonException;

class UserRepository extends BaseRepository implements JsonUserRepository
{
    /**
     * @param int $limit
     * @return UserCollection
     * @throws JsonException
     */
    final public function getUserList(int $limit): UserCollection
    {
        // $limit can be used to limit the result collection in the external api
        // in this case (json_placeholder), we dont have this option...
        $response = $this->client->get('users');
        $userList = $this->parseResponse($response);
        return UserCollection::fromPlaceholderData($userList);
    }

    /**
     * @param int $userId
     * @return User
     * @throws DataNotFoundException
     * @throws JsonException
     */
    final public function getUserById(int $userId): User
    {
        $response = $this->client->get('users/' . $userId);
        $userData = $this->parseResponse($response);
        if (empty($userData)) {
            throw new DataNotFoundException('This user does not exist');
        }

        return User::fromPlaceholderData($userData);
    }
}

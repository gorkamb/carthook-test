<?php

namespace Infrastructure\Repositories\JSONPlaceholder;

use GuzzleHttp\Client;
use JsonException;
use Psr\Http\Message\ResponseInterface;

class BaseRepository
{
    /** @var Client */
    public Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws JsonException
     */
    final public function parseResponse(ResponseInterface $response): array
    {
        return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
    }
}

<?php

namespace Infrastructure\Repositories\JSONPlaceholder;

use Domain\Entity\Post\PostCollection;
use Domain\Repositories\JsonPostRepository;

class PostRepository extends BaseRepository implements JsonPostRepository
{
    final public function getPostsByUserId(int $userId, int $limit): PostCollection
    {
        // $limit can be used to limit the result collection in the external api
        // in this case (json_placeholder), we dont have this option...
        $query = [
            'userId' => $userId
        ];
        $response = $this->client->get('posts?' . http_build_query($query));
        $posts = $this->parseResponse($response);
        return PostCollection::fromPlaceholderData($posts);
    }
}
<?php

namespace Infrastructure\Repositories\Eloquent;

use Domain\Entity\Post\Post;
use Domain\Repositories\MysqlPostRepository;

class EloquentPostRepository implements MysqlPostRepository
{
    final public function save(Post $post): void
    {
        $post->save();
    }
}

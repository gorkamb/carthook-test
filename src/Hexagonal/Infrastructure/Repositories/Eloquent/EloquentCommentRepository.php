<?php

namespace Infrastructure\Repositories\Eloquent;

use Domain\Entity\Comment\Comment;
use Domain\Repositories\MysqlCommentRepository;

class EloquentCommentRepository implements MysqlCommentRepository
{
    final public function save(Comment $comment): void
    {
        $comment->save();
    }
}

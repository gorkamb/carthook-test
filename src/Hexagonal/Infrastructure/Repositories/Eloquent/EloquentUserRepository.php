<?php

namespace Infrastructure\Repositories\Eloquent;

use Domain\Entity\User\User;
use Domain\Repositories\MysqlUserRepository;

class EloquentUserRepository implements MysqlUserRepository
{
    final public function save(User $user): void
    {
        $user->save();
    }
}

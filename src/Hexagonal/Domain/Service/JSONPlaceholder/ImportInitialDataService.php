<?php

namespace Domain\Service\JSONPlaceholder;

use Domain\Entity\Comment\Comment;
use Domain\Entity\Post\Post;
use Domain\Entity\User\User;
use Domain\Repositories\JsonCommentRepository;
use Domain\Repositories\JsonPostRepository;
use Domain\Repositories\JsonUserRepository;
use Illuminate\Hashing\HashManager;
use Illuminate\Support\Str;
use Infrastructure\Repositories\Eloquent\EloquentCommentRepository;
use Infrastructure\Repositories\Eloquent\EloquentPostRepository;
use Infrastructure\Repositories\Eloquent\EloquentUserRepository;

class ImportInitialDataService
{
    /** @var JsonUserRepository */
    private JsonUserRepository $jsonUserRepository;

    /** @var JsonPostRepository */
    private JsonPostRepository $jsonPostRepository;

    /** @var JsonCommentRepository */
    private JsonCommentRepository $jsonCommentRepository;

    /** @var EloquentUserRepository */
    private EloquentUserRepository $userRepository;

    /** @var EloquentPostRepository */
    private EloquentPostRepository $postRepository;

    /** @var EloquentCommentRepository */
    private EloquentCommentRepository $commentRepository;
    /** @var HashManager */
    private HashManager $hashManager;

    public function __construct(
        JsonUserRepository $jsonUserRepository,
        JsonPostRepository $jsonPostRepository,
        JsonCommentRepository $jsonCommentRepository,
        EloquentUserRepository $userRepository,
        EloquentPostRepository $postRepository,
        EloquentCommentRepository $commentRepository,
        HashManager $hashManager

    ) {
        $this->jsonUserRepository = $jsonUserRepository;
        $this->jsonPostRepository = $jsonPostRepository;
        $this->jsonCommentRepository = $jsonCommentRepository;
        $this->userRepository = $userRepository;
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->hashManager = $hashManager;
    }

    final public function execute(): void
    {
        /** @var User[] $users */
        $users = $this->jsonUserRepository->getUserList(10);
        foreach ($users as $user) {
            $user->setPassword($this->hashManager->make(Str::random(20)));
            $this->userRepository->save($user);
            $this->importPostsAndComments($user);
        }
    }

    private function importPostsAndComments(User $user): void
    {
        /** @var Post[] $posts */
        $posts = $this->jsonPostRepository->getPostsByUserId($user->id(), 50);
        foreach ($posts as $post) {
            $this->postRepository->save($post);
            $this->importComments($post);
        }
    }

    private function importComments(Post $post): void
    {
        /** @var Comment[] $comments */
        // Added limit 50 for comments...
        $comments = $this->jsonCommentRepository->getCommentsByPostId($post->id(), 50);
        foreach ($comments as $comment) {
            $this->commentRepository->save($comment);
        }
    }
}
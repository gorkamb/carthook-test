<?php

namespace Domain\Entity\Post;

use Domain\Collection;

class PostCollection extends Collection
{
    public static function fromPlaceholderData(array $postListData): self
    {
        $collection = new self();
        foreach ($postListData as $postData) {
            $collection->add(Post::fromPlaceholderData($postData));
        }

        return $collection;
    }
}

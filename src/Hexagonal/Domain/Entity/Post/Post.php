<?php

namespace Domain\Entity\Post;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'posts';

    // region getters/setters
    public function id(): int
    {
        return $this->getAttributeValue('id');
    }

    public function setId(int $id): self
    {
        return $this->setAttribute('id', $id);
    }

    public function userId(): int
    {
        return $this->getAttributeValue('user_id');
    }

    public function setUserId(int $userId): self
    {
        return $this->setAttribute('user_id', $userId);
    }

    public function title(): string
    {
        return $this->getAttributeValue('title');
    }

    public function setTitle(string $title): self
    {
        return $this->setAttribute('title', $title);
    }

    public function body(): string
    {
        return $this->getAttributeValue('body');
    }

    public function setBody(string $body): self
    {
        return $this->setAttribute('body', $body);
    }
    // endregion getters/setters


    public static function fromPlaceholderData(array $postData): self
    {
        return (new self())
            ->setId($postData['id'])
            ->setUserId($postData['userId'])
            ->setTitle($postData['title'])
            ->setBody($postData['body']);
    }
}

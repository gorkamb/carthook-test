<?php

namespace Domain\Entity\User;

use Domain\Collection;

class UserCollection extends Collection
{
    public static function fromPlaceholderData(array $userListData): self
    {
        $collection = new self();
        foreach ($userListData as $userData) {
            $collection->add(User::fromPlaceholderData($userData));
        }

        return $collection;
    }
}

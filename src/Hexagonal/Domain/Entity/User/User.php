<?php

namespace Domain\Entity\User;

use Domain\ValueObject\Address;
use Domain\ValueObject\Company;
use Domain\ValueObject\Geo;

class User extends \App\User
{
    // region getters/setters

    public function id(): int
    {
        return $this->getAttributeValue('id');
    }

    public function setId(int $id): self
    {
        return $this->setAttribute('id', $id);
    }

    public function name(): string
    {
        return $this->getAttributeValue('name');
    }

    public function setName(string $name): self
    {
        return $this->setAttribute('name', $name);
    }

    public function username(): string
    {
        return $this->getAttributeValue('username');
    }

    public function setUsername(string $username): self
    {
        return $this->setAttribute('username', $username);
    }

    public function email(): string
    {
        return $this->getAttributeValue('email');
    }

    public function setEmail(string $email): self
    {
        return $this->setAttribute('email', $email);
    }

    public function password(): string
    {
        return $this->getAttributeValue('password');
    }

    public function setPassword(string $password): self
    {
        return $this->setAttribute('password', $password);
    }

    public function phone(): string
    {
        return $this->getAttributeValue('phone');
    }

    public function setPhone(string $phone): self
    {
        return $this->setAttribute('phone', $phone);
    }

    public function website(): string
    {
        return $this->getAttributeValue('website');
    }

    public function setWebsite(string $website): self
    {
        return $this->setAttribute('website', $website);
    }

    // endregion getters/setters

    // region relations
    public function address(): Address
    {
        return $this->getRelationValue('address');
    }

    public function company(): Company
    {
        return $this->getRelationValue('company');
    }

    // endregion relations

    public static function fromPlaceholderData(array $userData): self
    {
        return (new self())
            ->setId($userData['id'])
            ->setName($userData['name'])
            ->setUsername($userData['username'])
            ->setEmail($userData['email'])
            ->setPhone($userData['phone'])
            ->setWebsite($userData['website'])
            ->setRelation('address', Address::fromPlaceholderData($userData['address']))
            ->setRelation('company', Company::fromPlaceholderData($userData['company']));
    }
}

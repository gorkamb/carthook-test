<?php

namespace Domain\Entity\Comment;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // region getters/setters
    public function id(): int
    {
        return $this->getAttributeValue('id');
    }

    public function setId(int $id): self
    {
        return $this->setAttribute('id', $id);
    }

    public function postId(): int
    {
        return $this->getAttributeValue('post_id');
    }

    public function setPostId(int $id): self
    {
        return $this->setAttribute('post_id', $id);
    }

    public function name(): string
    {
        return $this->getAttributeValue('name');
    }

    public function setName(string $name): self
    {
        return $this->setAttribute('name', $name);
    }

    public function email(): string
    {
        return $this->getAttributeValue('email');
    }

    public function setEmail(string $email): self
    {
        return $this->setAttribute('email', $email);
    }

    public function body(): string
    {
        return $this->getAttributeValue('body');
    }

    public function setBody(string $body): self
    {
        return $this->setAttribute('body', $body);
    }

    // endregion getters/setters

    public static function fromPlaceholderData(array $commentData): self
    {
        return (new self())
            ->setId($commentData['id'])
            ->setPostId($commentData['postId'])
            ->setName($commentData['name'])
            ->setEmail($commentData['email'])
            ->setBody($commentData['body']);
    }
}

<?php

namespace Domain\Entity\Comment;

use Domain\Collection;

class CommentCollection extends Collection
{
    public static function fromPlaceholderData(array $commentListData): self
    {
        $collection = new self();
        foreach ($commentListData as $commentData) {
            $collection->add(Comment::fromPlaceholderData($commentData));
        }

        return $collection;
    }
}

<?php

namespace Domain\ValueObject;

class Company
{
    private string $name;
    private string $catchPhrase;
    private string $bs;

    public function __construct(
        string $name,
        string $catchPhrase,
        string $bs
    ) {
        $this->name = $name;
        $this->catchPhrase = $catchPhrase;
        $this->bs = $bs;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function catchPhrase(): string
    {
        return $this->catchPhrase;
    }

    public function bs(): string
    {
        return $this->bs;
    }

    public static function fromPlaceholderData(array $placeholderData): self
    {
        return new self(
            $placeholderData['name'],
            $placeholderData['catchPhrase'],
            $placeholderData['bs']
        );
    }
}

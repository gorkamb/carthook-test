<?php

namespace Domain\ValueObject;

class Geo
{
    private float $lat;
    private float $lng;

    public function __construct(float $lat, float $lng)
    {
        $this->lat = $lat;
        $this->lng = $lng;
    }

    public function lat(): float
    {
        return $this->lat;
    }

    public function lng(): float
    {
        return $this->lng;
    }

    public static function fromPlaceholderData(array $placeholderData): Geo
    {
        return new self(
            $placeholderData['lat'],
            $placeholderData['lng']
        );
    }
}

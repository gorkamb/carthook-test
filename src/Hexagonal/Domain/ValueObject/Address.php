<?php

namespace Domain\ValueObject;

class Address
{
    private string $street;
    private string $suite;
    private string $city;
    private string $zipcode;

    /** @var Geo */
    private Geo $geo;

    public function __construct(
        string $street,
        string $suite,
        string $city,
        string $zipcode,
        Geo $geo
    ) {
        $this->street = $street;
        $this->suite = $suite;
        $this->city = $city;
        $this->zipcode = $zipcode;
        $this->geo = $geo;
    }

    public function street(): string
    {
        return $this->street;
    }

    public function suite(): string
    {
        return $this->suite;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function zipcode(): string
    {
        return $this->zipcode;
    }

    public function geo(): Geo
    {
        return $this->geo;
    }

    public static function fromPlaceholderData(array $placeholderData): self
    {
        return new self(
            $placeholderData['street'],
            $placeholderData['suite'],
            $placeholderData['city'],
            $placeholderData['zipcode'],
            Geo::fromPlaceholderData($placeholderData['geo'])
        );
    }
}

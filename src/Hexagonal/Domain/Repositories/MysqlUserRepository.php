<?php

namespace Domain\Repositories;

use Domain\Entity\User\User;

interface MysqlUserRepository
{
    public function save(User $user): void;
}

<?php

namespace Domain\Repositories;

use Domain\Entity\Comment\Comment;

interface MysqlCommentRepository
{
    public function save(Comment $comment): void;
}

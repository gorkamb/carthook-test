<?php

namespace Domain\Repositories;

use Domain\Entity\User\User;
use Domain\Entity\User\UserCollection;

interface JsonUserRepository
{
    public function getUserList(int $limit): UserCollection;
    public function getUserById(int $userId): User;
}

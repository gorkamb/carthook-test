<?php

namespace Domain\Repositories;

use Domain\Entity\Comment\CommentCollection;

interface JsonCommentRepository
{
    public function getCommentsByPostId(int $postId, int $limit): CommentCollection;
}
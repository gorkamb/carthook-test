<?php

namespace Domain\Repositories;

use Domain\Entity\Post\PostCollection;

interface JsonPostRepository
{
    public function getPostsByUserId(int $userId, int $limit): PostCollection;
}
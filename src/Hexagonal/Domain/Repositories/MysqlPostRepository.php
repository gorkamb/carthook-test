<?php

namespace Domain\Repositories;

use Domain\Entity\Post\Post;

interface MysqlPostRepository
{
    public function save(Post $post): void;
}

<?php

namespace Application\Service\User;

use Domain\Entity\Post\PostCollection;
use Domain\Repositories\JsonPostRepository;

class GetUserPostsUseCase
{
    private JsonPostRepository $jsonPostRepository;

    public function __construct(JsonPostRepository $jsonPostRepository)
    {
        $this->jsonPostRepository = $jsonPostRepository;
    }

    final public function execute(int $userId, int $limit): PostCollection
    {
        return $this->jsonPostRepository->getPostsByUserId($userId, $limit);
    }
}

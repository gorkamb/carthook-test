<?php

namespace Application\Service\User;

use Domain\Entity\User\User;
use Domain\Repositories\JsonUserRepository;

class GetUserUseCase
{
    private JsonUserRepository $jsonUserRepository;

    public function __construct(JsonUserRepository $jsonUserRepository)
    {
        $this->jsonUserRepository = $jsonUserRepository;
    }

    final public function execute(int $userId): User
    {
        return $this->jsonUserRepository->getUserById($userId);
    }
}

<?php

namespace Application\Service\User;

use Domain\Entity\User\UserCollection;
use Domain\Repositories\JsonUserRepository;

class GetUserListUseCase
{
    private JsonUserRepository $jsonUserRepository;

    public function __construct(JsonUserRepository $jsonUserRepository)
    {
        $this->jsonUserRepository = $jsonUserRepository;
    }

    final public function execute(int $limit): UserCollection
    {
        return $this->jsonUserRepository->getUserList($limit);
    }
}

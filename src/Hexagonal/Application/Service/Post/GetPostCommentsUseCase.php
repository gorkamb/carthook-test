<?php

namespace Application\Service\Post;

use Domain\Entity\Comment\CommentCollection;
use Domain\Repositories\JsonCommentRepository;

class GetPostCommentsUseCase
{
    /** @var JsonCommentRepository */
    private JsonCommentRepository $jsonCommentRepository;

    public function __construct(JsonCommentRepository $jsonCommentRepository)
    {
        $this->jsonCommentRepository = $jsonCommentRepository;
    }

    final public function execute(int $postId, int $limit): CommentCollection
    {
        return $this->jsonCommentRepository->getCommentsByPostId($postId, $limit);
    }
}

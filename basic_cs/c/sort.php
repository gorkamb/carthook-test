<?php

/**
 * ESTIMATED values...
 * Base on this calculation in the 7º iteration we are in 8s. In next iterations we have to multiply almost x 10
 * So in the 10º iteration we have 100.000 seconds more than 1 day
 *
 * @return array
 * @throws Exception
 */
function randomNumbers()
{
    $numbers = [];
    for ($i = 0; $i < 11; $i++) {
        $numbers[] = rand(0, 100); // not secure than random_int but faster
    }
    return $numbers;
}

function sortNumbers()
{
    $numbers = randomNumbers();
    sort($numbers);
}

$start = microtime(true);

// current 10**8
for ($i = 0; $i < 10 ** 8; $i++) {
    sortNumbers();
}
echo number_format(microtime(true) - $start, 2) . ' seconds';

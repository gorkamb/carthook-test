<?php

/**
 * ESTIMATED values...
 * PHP_FLOAT_MAX exceeded
 * Base on this calculation in the 7º iteration we are in 12s. In next iterations we have to multiply almost x 11
 * So in the 10º iteration we have 175.000 seconds more than 2 days
 *
 * @return array
 * @throws Exception
 */
function randomNumbers(): array
{
    $numbers = [];
    for ($i = 0; $i < 100; $i++) {
        $a = rand(100, 10000);
        $b = rand(100, 10000);
        $numbers[] = $a ** $b; // INF value...
    }
    return $numbers;
}

function sortNumbers()
{
    $numbers = randomNumbers();
    sort($numbers);
}

$start = microtime(true);

// current 10**8
for ($i = 0; $i < 10 ** 8; $i++) {
    sortNumbers();
}
echo number_format(microtime(true) - $start, 2) . ' seconds';
